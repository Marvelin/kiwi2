 $(document).ready(function() {
     /*tab toogle*/
     $('.partners-tab span').click(function(){
         $('.partners-tab li').removeClass("active");
         $('.tab-panel').removeClass("active");  
         $(this).parent().addClass("active");
         var dataTargetValue = $(this).attr('data-target');
//         console.log(dataTargetValue);
         $(" " + dataTargetValue + " ").addClass("active");
         
     });
     //carusel
     $('.main-carusel_carusel').slick({
         dots: false,
         arrows: false,
         infinite: true,
         speed: 300,
         slidesToShow: 5,
         slidesToScroll: 1,
         speed: 500,
//         fade: true,
//         cssEase: 'linear',
//         centerPadding: '0',
         responsive: [
             
             {
                 breakpoint: 1200,
                 settings: {
                     slidesToShow: 4,
                 }
             },
             {
                 breakpoint: 1021,
                 settings: {
                     slidesToShow: 3,
                 }
             },
             {
                 breakpoint: 768,
                 settings: {
                     slidesToShow: 2
                 }
             },
             {
                 breakpoint: 600,
                 settings: {
                     slidesToShow: 1
                 }
             }
             // You can unslick at a given breakpoint now by adding:
             // settings: "unslick"
             // instead of a settings object
         ]
     }); 
     
     $('.sliders2').slick({
         dots: false,
         infinite: true,
         speed: 500,
         fade: true,
         touchMove: false,
         cssEase: 'linear',
         prevArrow: '<div class="arrow prew"></div>',
         nextArrow: '<div class="arrow next"></div>',
         responsive: [

             {
                 breakpoint: 1300,
                 settings: {
                     slidesToShow: 1,
                 }
             },
             {
                 breakpoint: 1021,
                 settings: {
                     slidesToShow: 1,
                     touchMove: true,
                     
                 }
             }
         ]
     });
     /* main slider image rotate*/
     setInterval (function(){
         $( ".normalization" ).each(function() {
             var rand = 1 - 0.5 + Math.random() * (4 - 1 + 1);
             rand = Math.round(rand);
             if( rand > 3) {
                 $( this ).toggleClass("hover");
             };
         });
     }, 5000);  
     
     
 });


  